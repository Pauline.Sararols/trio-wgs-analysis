###   -----------------   ###
###                       ###
###   WGS trio analysis   ###
###                       ###
###   -----------------   ###

example sample name: WGS_01_1, where:
- "01" is the family id 
- "1" is the individual id

For each trio:
- 1: proband
- 2: father
- 3: mother

### *** SAMPLE PRE-PROCESSING *** ###

### Step 1: FastQC ###
(For each fastq file)
Tool: FastQC
Input: ${sample}_R1.fastq.gz (*.fastq.gz)
Output: ${sample}_R1_fastqc.zip ${sample}_R1_fastqc.html
Script: 1-fastQC.sh


### Step 2:  mapping  ### (~ 10/26h)
# For each sample
Tool: Burrows Wheeler Aligner (BWA) + samtools
Input: ${sample}_R1.fastq.gz + ${sample}_R2.fastq.gz + indexed reference genome (fasta file + fai)
Output: sorted bam file ${sample}.bam + bam index ${sample}.bam.bai + ${sample}.CovStats
Script: 2-mapping.sh

# Intermediate file: ${sample}.unsorted.bam


### Step 3: Add RG Tag (optional, required for GATK4) ### (~2/4h)
# For each sample 
Tool: Picard
Input: sorted bam file ${sample}.bam
Output: sorted bam file with RG tag added ${sample}.RG.bam
Script: 3-Add_RG_Picard.sh


### Step 4: Remove duplicates reads (optical and PCR) ### (~4/8hours)
# For each sample 
Tool: Picard + samtools
Input: sorted bam file with RG tag added ${sample}.RG.bam
Output: sorted bam file with RG tag and no duplicate reads ${sample}.RG.RD.bam + bam index ${sample}.RG.RD.bam.bai
Script: 4-Mark_Remove_Duplicates.sh


### Step 5: Base recalibration (optional) ### (~8h)
# For each sample 
Tool: GATK4 + samtools
Input: sorted bam file with RG tag and no duplicate reads ${sample}.RG.RD.bam
Output: recalibration report ${sample}_recal_data.report + recalibrated sorted bam file with RG tag, no duplicate reads ${sample}.final.bam + bam index ${sample}.final.bam.bai
Script: 5-Base_recalibration.sh


### *** Step 6: Variant calling *** ###


### CNVs calling ###

### Manta (to run before Strelka)(~2h/6h)
# For each sample 
Tool: Manta
Input: final bam file + indexed reference genome (fasta file + fai)
Output: SV calls -> candidateSmallIndels.vcf.gz + candidateSV.vcf.gz + diploidSV.vcf.gz 
Script: 6-Variant_Calling_Manta.sh


### Delly (~3h/6h)
Tool: Delly
Input: final bam file + indexed reference genome (fasta file + fai)
Output: SV calls -> ${sample}.delly.vcf
Script: 6-Variant_Calling_Delly.sh


### CNVnator (~2h/3h)
Tool: CNVnator
Input: final bam file + splitted reference genome (fasta file + fai) in a folder
Output: SV calls -> ${sample}_CNVnator.vcf + ${sample}_CNVnator_${bin}.txt + ${sample}.root
Script: 6-Variant_Calling_CNVnator.sh


### ERDS (~2h/3h)
Tool: ERDS
Input: final bam file + indexed reference genome (fasta file + fai) + decomposed vcf file with SNV calls from DeepVariant ${sample}.variants.decomposed.vcf
Output: SV calls -> 1.erds.vcf + 1.events + 1.del.events + 1.dup.events
Script: 6-Variant_Calling_ERDS.sh


### Merge CNV calls of the four callers ###

### Survivor (~2h/6h)
# Create a text file with the names and paths of the vcf to merge (WGS-01-1_vcf_files.txt)
Tool: Survivor
Input: text file containing path of vcf files (CNVnator + Manta + Delly + ERDS)
Output: merge SV calls -> ${sample}_merged_CNV.vcf
Script: 6bis-CNV_merging_callers.sh


### Merge CNV calls of the four callers ###

### Survivor (~2h/6h)
# Create a text file with the names and paths of the vcf to merge (trio)
Tool: Survivor
Input: text file containing path of vcf files (${proband}_merged_CNV.vcf + ${father}_merged_CNV.vcf + ${mother}_merged_CNV.vcf)
Output: merge SV calls -> ${fam}_merged_CNV.vcf
Script: 6ter-CNV_merging_samples.sh


### CNVs annotation ###
### Survivor (~2h/6h)
# Create a text file with the names and paths of the vcf to merge (trio)
Tool: Survivor
Input: text file containing path of vcf files (${proband}_merged_CNV.vcf + ${father}_merged_CNV.vcf + ${mother}_merged_CNV.vcf)
Output: merge SV calls -> ${fam}_merged_CNV.vcf
Script: 6ter-CNV_merging_samples.sh

### SNVs calling ###

### DeepVariant (~12/20h) 
# For single sample (before GLnexus)
Tool: DeepVariant + Singularity
Input: sorted bam file with RG tag and no duplicate reads ${sample}.RG.RD.bam + indexed reference genome (fasta file + fai)
Output: variant calling for SNVs/indels (vcf.gz + g.vcf.gz)
Script: 6-Variant_calling_DeepVariant.sh

# GLNexus: merge proband/father/mother calls (~1h)
# For trio analysis: merge vcf for trio analysis
Tool: GLNexus + Singularity + Bcftools
Input: variant calling for SNVs/indels (g.vcf.gz) for proband-father-mother
Output: joint bcf file + joint vcf.gz file
Script: 6bis-Variant_Calling_DeepVariant_GLNexus.sh


### Strelka (~10h/16h)
# /!\ Run Manta before to call indels /!\

# For single sample analysis
Tool: Strelka
Input: final bam file + manta candidateSmallIndels.vcf.gz file + indexed reference genome (fasta file + fai)
Output: variant calling for SNVs/indels (vcf.gz)
Script: 6-Variant_Calling_Strelka.sh 

# For trio analysis (can be run directly)
Tool: Strelka
Input: proband final bam file + father final bam file + mother final bam file + indexed reference genome (fasta file + fai) +
       manta candidateSmallIndels.vcf.gz file for proband + manta candidateSmallIndels.vcf.gz file for father + manta candidateSmallIndels.vcf.gz file for mother    
Output: joint variant calling for SNVs/indels (vcf.gz)
Script: 6-Variant_Calling_Strelka_trio.sh



### *** Step 7: SNVs annotation *** ###

# WGS annotation (single sample or trio) (~2h/6h)
Tool: Annovar + bcftools
Input: output vcf from DeepVariant (${sample}.vcf.gz)
Output: decomposed vcf file (${sample}.variants.decomposed.vcf)+ OMIM annotated vcf file (${sample}.variants.decomposed.OMIM.vcf) + annotated vcf (${sample}.hg38_multianno.vcf)
Script: 7-Variant_annotation_DeepVariant.sh for single sample annotation
Script: 7-Variant_annotation_trio_DeepVariant.sh for trio annotation


# WGS annotation of Regulatory elements (RE) (~1h)
# Subset variants in RE 
Tool: vcftools + Annovar + bcftools
Input: decomposed vcf file (${sample}.variants.decomposed.vcf) 
Output: annotated variant calls in RE -> vcf with SNVs calls (${sample}.SNVs.ENCODE.vcf) + vcf with indels calls (${sample}.indels.ENCODE.vcf) + vcf with all calls (${sample}.variants.ENCODE.vcf.gz) + annotated vcf (${sample}_ENCODE.hg38_multianno.vcf)
Script: 7-Variant_annotation_RE.sh


# WGS annotation for imprinted genes (~1h)
# Subset variants in imprinted genes 
Tool: vcftools + Annovar + bcftools
Input: decomposed vcf file (${sample}.variants.decomposed.vcf) 
Output: annotated variants calls in imprinted genes -> vcf (${sample}.variants.decomposed.IMPRINTING.vcf) + annotated vcf (${sample}_imprinted_variants.hg38_multianno.vcf)
Script: 7-Variant_annotation_trio_DeepVariant_imprinted_genes.sh


# WGS annotation of trio genotype (trio) (~15min)
Tool: Slivar
Input: vcf annotated with Annovar (${sample}.hg38_multianno.vcf) + pedigree file
Output: filtered vcf with trio annotation (${sample}_variants_annotated.vcf)
Script: 8-Variant_annotation_trio_slivar.sh


# WES analysis (Whole Exome) (~5min)
# Subset WES variants in Twist Comprehensive exome capture 
Tool: vcftools
Input: annotated vcf (${sample}_variants_annotated.vcf) + bed file of Exome coordinates (Twist_Comprehensive_Exome_Covered_Targets_hg38.bed)
Output: subsetted vcf (${sample}_WES_variants.recode.vcf)
Script: 9-WES_variant_annotation_subsetting.sh 


# Convertion into table (~5min)
Tool: GATK4
Input: annotated vcf (${sample}_variants_annotated.vcf)
Output: annotated table (${sample}_variants_annotated.tsv) with selected columns
Script: 10-Conversion_to_table.sh 



### Short Tandem Repeats (STR) expansion ###

### Expansionhunter (~5min)
Tool: ExpansionHunter
Input: sorted bam file with RG tag and no duplicate reads ${sample}.RG.RD.bam + indexed reference genome (fasta file + fai) + STR catalog (json)  
Output: variant calling for STR expansion (nb of repeats) (vcf + json format) + realigned bam file
Script: 6-Expansionhunter.sh 



