#!/bin/bash
#SBATCH --job-name ExpHunter
#SBATCH --error ExpHunter-error.e%j
#SBATCH --output ExpHunter-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 16
#SBATCH --mem=60000  
#SBATCH --mail-type=all          # send email on job start, end and fault

sample="WGS-XX-X"

bam_path="/home/users/s/sararols/scratch/DDID_data/Bam"
resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38"
results="/home/users/s/sararols/scratch/DDID_data/ExpansionHunter/${sample}"

cd ${results}

${HOME}/bin/ExpansionHunter-v5.0.0/bin/ExpansionHunter --reads ${bam_path}/${sample}/${sample}.RG.RD.bam \
                --reference ${resources}/chm13v2.0_maskedY.fa \
                --variant-catalog ${HOME}/bin/ExpansionHunter-v5.0.0/variant_catalog/hg38/variant_catalog.json \
                --output-prefix ${sample} \
                --aligner dag-aligner \
                --analysis-mode seeking \
                --threads 16

