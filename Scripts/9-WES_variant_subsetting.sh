#!/bin/bash
#SBATCH --job-name WES_subsetting
#SBATCH --error WES_subsetting-error.e%j
#SBATCH --output WES_subsetting_db-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 11:00:00
#SBATCH --mem=32000              # 32Go
#SBATCH --mail-type=all          # send email on job start, end and fault

module load GCC/8.3.0 VCFtools/0.1.16

sample="FamXX"

cd /home/users/s/sararols/scratch/DDID_data/DeepVariant/${sample}

vcftools --vcf ${sample}_variants_annotated.vcf --bed /home/users/s/sararols/scratch/DDID_data/Resources/Twist_Comprehensive_Exome_Covered_Targets_hg38.bed --out ${sample}_WES_variants --recode --keep-INFO-all