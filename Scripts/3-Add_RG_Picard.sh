#!/bin/bash
#SBATCH --job-name RGPicard
#SBATCH --error RGPicard-error.e%j
#SBATCH --output RGPicard-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 10:00:00
#SBATCH --cpus-per-task 16
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

module purge
#module load GCC/8.3.0 SAMtools/1.10
module load picard/2.21.1-Java-11

sample="WGS-XX-X"

cd /home/users/s/sararols/scratch/DDID_data/Bam/${sample}

# Sort Bam file
#samtools sort ${sample}.unsorted.bam -o ${sample}.bam
#samtools index ${sample}.bam
#samtools stats ${sample}.bam > ${sample}.CovStats

# Add read group @RG

IFS='-' read -r -a array <<< "${sample}"
echo ${array[1]}
echo ${array[2]}

java -jar $EBROOTPICARD/picard.jar AddOrReplaceReadGroups \
    I=${sample}.bam \
    O=${sample}.RG.bam \
    SORT_ORDER=coordinate \
    RGID=${array[1]} \
    RGPL=illumina \
    RGSM=${array[2]} \
    RGLB=lib \
    RGPU=WGS \
    CREATE_INDEX=true
