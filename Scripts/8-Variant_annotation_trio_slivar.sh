#!/bin/bash
#SBATCH --job-name Slivar
#SBATCH --error Slivar-error.e%j
#SBATCH --output Slivar-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 8
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

sample="FamXX"

# Go to the working directory
results_folder="/home/users/s/sararols/scratch/DDID_data/DeepVariant/${sample}"

cd $results_folder

resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38"

### 3. Run slivar

vcf="$results_folder/${sample}.hg38_multianno.vcf"                    # WGS
#vcf="$results_folder/${sample}_imprinted_variants.hg38_multianno.vcf" # Imprinted genes
#vcf="$results_folder/${sample}.variants.ENCODE.vcf"                   # Regulatory elements
ped="/home/users/s/sararols/scratch/DDID_data/pedigree.ped"
output="$results_folder/${sample}_variants_annotated.vcf"

#wget https://slivar.s3.amazonaws.com/gnomad.hg38.genomes.v3.fix.zip

$HOME/bin/slivar expr --js $HOME/bin/slivar-functions.js -g ${resources}/gnomad.hg38.genomes.v3.fix.zip \
    --vcf $vcf --ped $ped -o $output \
    --info 'variant.FILTER == "PASS" && variant.ALT[0] != "*"' \
    --family-expr 'x_denovo:(variant.CHROM == "X" || variant.CHROM == "chrX") && fam.every(segregating_denovo_x)' \
    --family-expr 'x_recessive:(variant.CHROM == "X" || variant.CHROM == "chrX") && fam.every(segregating_recessive)' \
    --trio 'comphet_side:comphet_side(kid, mom, dad)' \
    --trio "denovo:kid.het && mom.hom_ref && dad.hom_ref \
        && kid.AB > 0.25 && kid.AB < 0.75 \
        && (mom.AD[1] + dad.AD[1]) == 0 \
        && kid.GQ >= 20 && mom.GQ >= 20 && dad.GQ >= 20 \
        && kid.DP >= 10 && mom.DP >= 10 && dad.DP >= 10" \
    --trio "informative:kid.GQ > 20 && dad.GQ > 20 && mom.GQ > 20 && kid.alts == 1 && \
           ((mom.alts == 1 && dad.alts == 0) || (mom.alts == 0 && dad.alts == 1))" \
    --pass-only



