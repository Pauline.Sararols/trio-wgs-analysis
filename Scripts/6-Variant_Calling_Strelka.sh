#!/bin/bash
#SBATCH --job-name Strelka
#SBATCH --error Strelka-error.e%j
#SBATCH --output Strelka-out.o%j
#SBATCH --partition public-cpu
#SBATCH --time 1-00:00:00
#SBATCH --cpus-per-task=2        # CPU cores/threads
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

# Load Strelka
module load GCC/7.3.0-2.30 OpenMPI/3.1.1 strelka/2.9.10-Python-2.7.15

sample="WGS-XX-X"

# Set up analysis directories
STRELKA_INSTALL_PATH="/opt/ebsofts/strelka/2.9.10-foss-2018b-Python-2.7.15/"  # Ne pas modifier
STRELKA_ANALYSIS_PATH="/home/users/s/sararols/scratch/DDID_data/Strelka/${sample}"
BAM_PATH="/home/users/s/sararols/scratch/DDID_data/Bam/${sample}"
resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38/"
MANTA_ANALYSIS_PATH="/home/users/s/sararols/scratch/DDID_data/Manta/${sample}/results/variants"

# Set up anylsis pipeline
${STRELKA_INSTALL_PATH}/bin/configureStrelkaGermlineWorkflow.py \
--bam ${BAM_PATH}/${sample}.final.bam \
--referenceFasta ${resources}/Homo_sapiens_assembly38.fasta.fa \
--indelCandidates ${MANTA_ANALYSIS_PATH}/candidateSmallIndels.vcf.gz \
--runDir ${STRELKA_ANALYSIS_PATH}

${STRELKA_ANALYSIS_PATH}/runWorkflow.py -q shared-cpu -g 2 -m local