#!/bin/bash
#SBATCH --job-name Strelka_f9
#SBATCH --error Strelka_f9-error.e%j
#SBATCH --output Strelka_f9-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task=8        # CPU cores/threads
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault


# Load Strelka
module load GCC/7.3.0-2.30 OpenMPI/3.1.1 strelka/2.9.10-Python-2.7.15

# Samples for trio analysis
sample_01="WGS-XX-1"
sample_02="WGS-XX-2"
sample_03="WGS-XX-3"
fam="FamXX"

# Set up analysis directories
STRELKA_INSTALL_PATH="/opt/ebsofts/strelka/2.9.10-foss-2018b-Python-2.7.15/"  # Ne pas modifier
STRELKA_ANALYSIS_PATH="/home/users/s/sararols/scratch/DDID_data/Strelka/${fam}"
BAM_PATH="/home/users/s/sararols/scratch/DDID_data/Bam"
reference_genome="/home/users/s/sararols/scratch/DDID_data/Resources/hg38"
MANTA_ANALYSIS_PATH="/home/users/s/sararols/scratch/DDID_data/Manta"

# Set up anylsis pipeline
${STRELKA_INSTALL_PATH}/bin/configureStrelkaGermlineWorkflow.py \
--bam ${BAM_PATH}/${sample_01}/${sample_01}.final.bam \
--bam ${BAM_PATH}/${sample_02}/${sample_02}.final.bam \
--bam ${BAM_PATH}/${sample_03}/${sample_03}.final.bam \
--referenceFasta ${reference_genome}/Homo_sapiens_assembly38.fasta \
--indelCandidates ${MANTA_ANALYSIS_PATH}/${sample_01}/results/variants/candidateSmallIndels.vcf.gz \
--indelCandidates ${MANTA_ANALYSIS_PATH}/${sample_02}/results/variants/candidateSmallIndels.vcf.gz \
--indelCandidates ${MANTA_ANALYSIS_PATH}/${sample_03}/results/variants/candidateSmallIndels.vcf.gz \
--runDir ${STRELKA_ANALYSIS_PATH}

${STRELKA_ANALYSIS_PATH}/runWorkflow.py -q shared-cpu -g 8 -m local