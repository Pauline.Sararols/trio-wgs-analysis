#!/bin/bash
#SBATCH --job-name Annotation_RE
#SBATCH --error Annotation_RE-error.e%j
#SBATCH --output Annotation_RE-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 8
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

sample="FamXX"

# Go to the working directory
results_folder="/home/users/s/sararols/scratch/DDID_data/DeepVariant/${sample}/"
cd $results_folder

resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38"


### 1. Decompose vcf
# Already done 
# From ${sample}.variants.decomposed.vcf

### 2. Subset VCF with only RE elements 
module load GCC/8.3.0 VCFtools/0.1.16

# module load GCCcore/10.2.0 BEDTools/2.30.0

# ${sample}_RE_variants
#bgzip RefSeq_func_elements_hg38.bed
#tabix RefSeq_func_elements_hg38.bed.gz

# ${sample}_ENCODE_variants
# bedtools sort -i Encode_cCRE_hg38.bed > Encode_cCRE_hg38_sorted.bed (for SNVs) / 
# bgzip Encode_cCRE_hg38_sorted.bed
# tabix Encode_cCRE_hg38_sorted.bed.gz

# SNVs
vcftools --vcf ${sample}.variants.decomposed.OMIM.vcf --bed $resources/Encode_cCRE_hg38_sorted.bed --out ${sample}_ENCODE_SNVs --recode --keep-INFO-all --remove-indels

# ${sample}_ENCODE_variants
# bedtools sort -i Encode_cCRE_hg38_50bp.bed > Encode_cCRE_hg38_50bp_sorted.bed (for indels) / 
# bgzip Encode_cCRE_hg38_50bp_sorted.bed
# tabix Encode_cCRE_hg38_50bp_sorted.bed.gz

# Indels 
vcftools --vcf ${sample}.variants.decomposed.OMIM.vcf --bed $resources/Encode_cCRE_hg38_50bp_sorted.bed --out ${sample}_ENCODE_indels --recode --keep-INFO-all --keep-only-indels 

### 2. Annotation with Annovar

module purge
module load GCC/11.2.0 BCFtools/1.14

# SNVs
bcftools annotate \
  -a ${resources}/Encode_cCRE_hg38_sorted.bed.gz \
  -c CHROM,FROM,TO,RE_TYPE,RE_NAME \
  -h ${resources}/vcf_header_FuncElement.hdr \
  -o ${sample}.SNVs.ENCODE.vcf \
  ${sample}_ENCODE_SNVs.recode.vcf

bgzip ${sample}.SNVs.ENCODE.vcf
tabix ${sample}.SNVs.ENCODE.vcf.gz

# Indels
bcftools annotate \
  -a ${resources}/Encode_cCRE_hg38_50bp_sorted.bed.gz \
  -c CHROM,FROM,TO,RE_TYPE,RE_NAME \
  -h ${resources}/vcf_header_FuncElement.hdr \
  -o ${sample}.indels.ENCODE.vcf \
  ${sample}_ENCODE_indels.recode.vcf

bgzip ${sample}.indels.ENCODE.vcf
tabix ${sample}.indels.ENCODE.vcf.gz

bcftools concat -a -o ${sample}.variants.ENCODE.vcf.gz -O v ${sample}.SNVs.ENCODE.vcf.gz ${sample}.indels.ENCODE.vcf.gz 

gzip -f -d ${sample}.variants.ENCODE.vcf.gz

### For indels 

perl $HOME/bin/annovar/table_annovar.pl ${sample}.variants.ENCODE.vcf $HOME/bin/annovar/humandb/ -buildver hg38 -out ${sample}_ENCODE -remove \
     -protocol refGene,avsnp150,dbnsfp42c,gnomad30_genome,clinvar_20220320,dbscsnv11 -operation gx,f,f,f,f,f -arg '-hgvs',,,,, -nastring . -vcfinput -polish


#$HOME/bin/annovar/annotate_variation.pl Fam1.avinput $HOME/bin/annovar/humandb/ -bedfile hg38_cytoBand.txt -dbtype bed -colsWanted 4 -regionanno -out test
