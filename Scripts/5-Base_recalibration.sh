#!/bin/bash
#SBATCH --job-name Base_recal
#SBATCH --error Base_recal-error.e%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 16
#SBATCH --mem=60000              # 120Go
#SBATCH --mail-type=all          # send email on job start, end and fault
#SBATCH --mail-user=pauline.sararols@unige.ch


module load GCCcore/8.3.0 GATK/4.1.3.0-Java-1.8

cd /home/users/s/sararols/scratch/DDID_data/Bam/${sample}

#1. Compute a model to correct the data
gatk BaseRecalibrator \
   -I ${sample}.RG.RD.bam \
   -R /home/users/s/sararols/scratch/DDID_data/Resources/hg38/Homo_sapiens_assembly38.fasta \
   --known-sites /home/users/s/sararols/scratch/DDID_data/Resources/hg38/1000G_phase1.snps.high_confidence.hg38.vcf.gz \
   --known-sites /home/users/s/sararols/scratch/DDID_data/Resources/hg38/Homo_sapiens_assembly38.known_indels.vcf.gz \
   --known-sites /home/users/s/sararols/scratch/DDID_data/Resources/hg38/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz \
   -O ${sample}_recal_data.report


#2. Apply the model
gatk ApplyBQSR \
    -I ${sample}.RG.RD.bam \
    -R /home/users/s/sararols/scratch/DDID_data/Resources/T2T/chm13v2.0.fa \
    --bqsr-recal-file ${sample}_recal_data.report \
    -O ${sample}.final.bam

# Load SAMtools 
module purge
module load GCC/10.3.0 SAMtools/1.13

samtools index ${sample}.final.bam ${sample}.final.bam".bai"

