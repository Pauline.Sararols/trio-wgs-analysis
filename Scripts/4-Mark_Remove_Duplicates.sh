#!/bin/bash
#SBATCH --job-name MarkDup
#SBATCH --error MarkDup-error.e%j
#SBATCH --output MarkDup-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 16
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

# Load Picard tools
module load picard/2.21.1-Java-11

sample="WGS-XX-X"

cd /home/users/s/sararols/scratch/DDID_data/Bam/${sample}

java -jar $EBROOTPICARD/picard.jar MarkDuplicates \
	I=${sample}.RG.bam \
	O=${sample}.RG.RD.bam \
	M=${sample}_Mark_Duplicates_metrics.txt \
	REMOVE_DUPLICATES=true \
	ASSUME_SORTED=true

# Load SAMtools 
module load GCC/10.3.0 SAMtools/1.13

samtools index ${sample}.RG.RD.bam ${sample}.RG.RD.bam".bai"

