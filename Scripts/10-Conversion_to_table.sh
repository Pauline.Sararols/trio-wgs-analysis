#!/bin/bash
#SBATCH --job-name Convert2Table
#SBATCH --error Convert2Table-error.e%j
#SBATCH --output Convert2Table-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --cpus-per-task 1
#SBATCH --time 1:00:00
#SBATCH --mem=10000              # 10Go
#SBATCH --mail-type=all          # send email on job start, end and fault

# Load required modules
module load GCCcore/8.3.0 GATK/4.1.3.0-Java-1.8

sample="WGS-XX-X"

# Go to the working directory
results_folder="/home/users/s/sararols/scratch/DDID_data/DeepVariant/${sample}/"

cd $results_folder

gatk VariantsToTable \
     -R /home/users/s/sararols/scratch/DDID_data/Resources/hg38/Homo_sapiens_assembly38.fasta \
     -V ${sample}.hg38_multianno.vcf \
     -F CHROM -F POS -F REF -F ALT -F denovo -F cytoBand -F TYPE  \
     -F Gene.refGene -F GeneDetail.refGene -F Func.refGene -F ExonicFunc.refGene  -F AAChange.refGene -F avsnp150  \
     -F AF -F AF_male -F AF_female -F gnomad_popmax_af -F gnomad_nhomalt \
     -F INHERITANCE -F OMIM_INFO -F dbscsnv11 -F comphet_side -GF GT -F CLNSIG \
     -F SIFT_score -F SIFT_pred -F MutationTaster_score -F MutationTaster_pred -F FATHMM_score -F FATHMM_pred \
     -F LIST-S2_score -F LIST-S2_pred -F dbscSNV_ADA_SCORE -F dbscSNV_RF_SCORE -F GERP++_NR -F GERP++_RS \
     -O ${sample}_variants_annotated.tsv
