#!/bin/bash
#SBATCH --job-name DeepVariant
#SBATCH --error DeepVariant-error.e%j
#SBATCH --output DeepVariant-out.o%j
#SBATCH --partition public-cpu
#SBATCH --time 4-00:00:00
#SBATCH --cpus-per-task=16       # CPU cores/threads
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

module purge
module load GCC/9.3.0 Singularity/3.7.3-Go-1.14

# singularity pull docker://google/deepvariant

sample="WGS-XX-X"

# /!\ Singularity ne prend pas en compte les liens symboliques -> pathway complet du scratch ! /!\ 

cd /home/users/s/sararols/scratch/DDID_data/
INPUT_DIR="/scratch/DDID_data/Bam/${sample}"
OUTPUT_DIR="/scratch/DDID_data/DeepVariant/${sample}"
resources="/scratch/DDID_data/Resources/hg38"

# Run DeepVariant.
singularity exec --bind /home/users/s/sararols/scratch/:/scratch  \
		/home/users/s/sararols/bin/deepvariant_1.4.0.sif \
		/opt/deepvariant/bin/run_deepvariant \
        --intermediate_results_dir=/scratch/DDID_data/DeepVariant/${sample}/tmp/ \
        --model_type=WGS \
        --ref=${resources}/Homo_sapiens_assembly38.fasta \
        --reads=${INPUT_DIR}/${sample}.RG.RD.bam \
        --output_vcf=${OUTPUT_DIR}/${sample}.vcf.gz \
        --output_gvcf=${OUTPUT_DIR}/${sample}.g.vcf.gz \
        --num_shards=16
