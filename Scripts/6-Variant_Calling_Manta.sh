#!/bin/bash
#SBATCH --job-name Manta
#SBATCH --error Manta-error.e%j
#SBATCH --output Manta-out.o%j
#SBATCH --partition public-cpu
#SBATCH --time 3-00:00:00
#SBATCH --cpus-per-task 8
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault


# Load Manta
module load manta/1.6.0

sample="WGS-XX-X"

# Set up analysis directories
MANTA_INSTALL_PATH="/opt/ebsofts/manta/1.6.0/"
MANTA_ANALYSIS_PATH="/home/users/s/sararols/scratch/DDID_data/Manta_hg38/${sample}"
BAM_PATH="/home/users/s/sararols/scratch/DDID_data/Bam_hg38/${sample}"
reference_data="/home/users/s/sararols/scratch/DDID_data/Resources/hg38"

${MANTA_INSTALL_PATH}/bin/configManta.py \
	--bam ${BAM_PATH}/${sample}.final.bam \
	--exome \
	--referenceFasta ${reference_data}/Homo_sapiens_assembly38.fasta \
	--runDir ${MANTA_ANALYSIS_PATH}

${MANTA_ANALYSIS_PATH}/runWorkflow.py -j 8

### Convert inversion ###

### On computer (not working on Baobab)
#./convertInversion.py /home/pauline/bin/miniconda3/envs/WGS/bin/samtools /home/pauline/Documents/DDID_project/Resources/hg38/Homo_sapiens_assembly38.fasta diploidSV.vcf.gz > converted_diploidSV.vcf

