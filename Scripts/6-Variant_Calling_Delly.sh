#!/bin/bash
#SBATCH --job-name Delly
#SBATCH --error Delly-error.e%j
#SBATCH --output Delly-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 6:00:00
#SBATCH --cpus-per-task 16
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

# Load Delly
module load GCC/11.3.0 Delly/1.1.5

sample="WGS-XX-X"

# Set up analysis directories
ANALYSIS_PATH="/home/users/s/sararols/scratch/DDID_data/Delly/${sample}"
BAM_PATH="/home/users/s/sararols/scratch/DDID_data/Bam/${sample}"
resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38/"

delly call -g ${resources}/Homo_sapiens_assembly38.fasta ${BAM_PATH}/${sample}.final.bam > ${ANALYSIS_PATH}/${sample}.delly.vcf