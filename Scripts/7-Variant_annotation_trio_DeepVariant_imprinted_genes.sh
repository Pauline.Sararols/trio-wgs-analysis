#!/bin/bash
#SBATCH --job-name Annotation_DeepVariant
#SBATCH --error Annotation_DeepVariant-error.e%j
#SBATCH --output Annotation_DeepVariant-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 8
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

# Load required modules
module load GCC/11.2.0 BCFtools/1.14

sample="FamXX"

# Go to the working directory
results_folder="/home/users/s/sararols/scratch/DDID_data/DeepVariant/${sample}/"
cd $results_folder

resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38"

### 1. Decompose vcf

#zcat ${sample}.trio.deepvariant.vcf.gz \
#  | sed -e 's/ID=AD,Number=\./ID=AD,Number=R/' \
#  | bcftools norm -m - -w 10000 -f $resources/Homo_sapiens_assembly38.fasta -O v -o ${sample}.variants.decomposed.vcf
# Already done

### 2. Subset calls in imprinted genes 

module load GCC/8.3.0 VCFtools/0.1.16

vcftools --vcf ${sample}.variants.decomposed.vcf --bed $resources/imprinted_genes_coordinates_hg38.bed --out ${sample}_imprinted_variants --recode --keep-INFO-all

### 3. Annotation with Annovar

module purge
module load GCC/11.2.0 BCFtools/1.14

bcftools annotate \
  -a ${resources}/imprinted_genes_coordinates_hg38.sorted.bed.gz \
  -c CHROM,FROM,TO,EXPR_ALLELE, \
  -h ${resources}/vcf_header_imprinting.hdr \
  -o ${sample}.variants.decomposed.IMPRINTING.vcf \
  ${sample}_imprinted_variants.recode.vcf

perl $HOME/bin/annovar/table_annovar.pl ${sample}.variants.decomposed.IMPRINTING.vcf $HOME/bin/annovar/humandb/ -buildver hg38 -out ${sample}_imprinted_variants -remove \
     -protocol refGene,avsnp150,dbnsfp42c,gnomad30_genome,clinvar_20220320,dbscsnv11 -operation gx,f,f,f,f,f -arg '-hgvs',,,,, -nastring . -vcfinput -polish

