#!/bin/bash
#SBATCH --job-name GLNexus
#SBATCH --error GLNexus-error.e%j
#SBATCH --output GLNexus-out.o%j
#SBATCH --partition shared-bigmem
#SBATCH --time 6:00:00
#SBATCH --cpus-per-task=16       # CPU cores/threads
#SBATCH --mem=120000              # 120Go
#SBATCH --mail-type=all          # send email on job start, end and fault

module load GCC/9.3.0 Singularity/3.7.3-Go-1.14

# /!\ Singularity ne prend pas en compte les liens symboliques -> pathway complet du scratch ! /!\ 

sample_01="WGS-XX-1"
sample_02="WGS-XX-2"
sample_03="WGS-XX-3"
fam="FamXX"

INPUT_DIR="/scratch/DDID_data/DeepVariant"

cd /home/users/s/sararols/scratch/DDID_data/DeepVariant/${fam}

# Run DeepVariant.

# singularity pull docker://quay.io/mlin/glnexus:v1.2.7
# singularity exec --bind /home/users/s/sararols/scratch/:/scratch /home/users/s/sararols/bin/glnexus_v1.2.7.sif bash

singularity run --bind /home/users/s/sararols/scratch/:/scratch \
    /home/users/s/sararols/bin/glnexus_v1.2.7.sif \
    /usr/local/bin/glnexus_cli \
    --config DeepVariant \
    ${INPUT_DIR}/${sample_01}/${sample_01}.g.vcf.gz ${INPUT_DIR}/${sample_02}/${sample_02}.g.vcf.gz ${INPUT_DIR}/${sample_03}/${sample_03}.g.vcf.gz  \
    > ${fam}.trio.deepvariant.bcf

module purge
module load GCC/11.2.0 BCFtools/1.14
bcftools view ${fam}.trio.deepvariant.bcf | bgzip -c > ${fam}.trio.deepvariant.vcf.gz


#Usage: /usr/local/bin/glnexus_cli [options] /vcf/file/1 .. /vcf/file/N
#Merge and joint-call input gVCF files, emitting multi-sample BCF on standard output.

#Options:
#  --dir DIR, -d DIR              scratch directory path (mustn't already exist; default: ./GLnexus.DB)
#  --config X, -c X               configuration preset name or .yml filename (default: gatk)
#  --bed FILE, -b FILE            three-column BED file with ranges to analyze (if neither --range nor --bed: use full length of all contigs)
#  --list, -l                     expect given files to contain lists of gVCF filenames, one per line

#  --more-PL, -P                  include PL from reference bands and other cases omitted by default
#  --squeeze, -S                  reduce pVCF size by suppressing detail in cells derived from reference bands
#  --trim-uncalled-alleles, -a    remove alleles with no output GT calls in postprocessing

#  --mem-gbytes X, -m X           memory budget, in gbytes (default: most of system memory)
#  --threads X, -t X              thread budget (default: all hardware threads)

#  --help, -h                     print this help message

#Configuration presets:
#            Name          CRC32C    Description
#            gatk       268790301    Joint-call GATK-style gVCFs
# gatk_unfiltered       484625853    Merge GATK-style gVCFs with no QC filters or genotype revision
#          xAtlas      3445896276    Joint-call xAtlas gVCFs
#xAtlas_unfiltered       920229760   Merge xAtlas gVCFs with no QC filters or genotype revision
#          weCall      2936345659    Joint-call weCall gVCFs
#weCall_unfiltered      2838640462   Merge weCall gVCFs with no filtering or genotype revision
#     DeepVariant      2857227159    Joint call DeepVariant whole genome sequencing gVCFs
#  DeepVariantWGS      2857227159    Joint call DeepVariant whole genome sequencing gVCFs
#  DeepVariantWES      4105299981    Joint call DeepVariant whole exome sequencing gVCFs
#DeepVariant_unfiltered       116393675  Merge DeepVariant gVCFs with no QC filters or genotype revision
#        Strelka2      3838963651    [EXPERIMENTAL] Merge Strelka2 gVCFs with no QC filters or genotype revision
