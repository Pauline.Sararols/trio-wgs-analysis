#!/bin/bash
#SBATCH --job-name Annotation_DeepVariant
#SBATCH --error Annotation_DeepVariant-error.e%j
#SBATCH --output Annotation_DeepVariant-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 8
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

# Load required modules
module load GCC/11.2.0 BCFtools/1.14

sample="WGS-XX-X"

# Go to the working directory
results_folder="/home/users/s/sararols/scratch/DDID_data/DeepVariant/${sample}"
cd $results_folder

resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38"

### 1. Decompose vcf

zcat ${sample}.vcf.gz \
  | sed -e 's/ID=AD,Number=\./ID=AD,Number=R/' \
  | bcftools norm -m - -w 10000 -f $resources/Homo_sapiens_assembly38.fasta -O v -o ${sample}.variants.decomposed.vcf

### 2. Annotation with Annovar

bcftools annotate \
  -a ${resources}/OMIM_annotation.bed.gz \
  -c CHROM,FROM,TO,OMIM_INFO,INHERITANCE \
  -h ${resources}/vcf_header.hdr \
  -o ${sample}.variants.decomposed.OMIM.vcf \
  ${sample}.variants.decomposed.vcf

perl $HOME/bin/annovar/table_annovar.pl ${sample}.variants.decomposed.OMIM.vcf $HOME/bin/annovar/humandb/ -buildver hg38 -out ${sample} -remove \
     -protocol refGene,avsnp150,dbnsfp42c,gnomad30_genome,clinvar_20220320,dbscsnv11 -operation gx,f,f,f,f,f -arg '-hgvs',,,,, -nastring . -vcfinput -polish
