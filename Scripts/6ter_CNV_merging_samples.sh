#!/bin/bash
#SBATCH --job-name Merge_samples
#SBATCH --error Merge_samples-error.e%j
#SBATCH --output Merge_samples-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 11:00:00
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

module load Anaconda3/5.3.0
. /opt/ebsofts/Core/Anaconda3/5.3.0/etc/profile.d/conda.sh
conda activate Survivor

### Merge samples with Survivor ###

fam="FamXX"

cd /home/users/s/sararols/scratch/DDID_data/Survivor/${fam}

# 1. Create a txt file with vcf files to merge

# 2. Run Survivor 

SURVIVOR merge ${fam}_vcf_files.txt 500 1 1 1 1 50 ${fam}_merged_CNV.vcf

#SURVIVOR merge sample_files.vcf 1000 2 1 1 (0) 30 sample_merged.vcf
#1000: maximum allowed distance of 1kb, as measured pairwise between breakpoints (begin1 vs begin2, end1 vs end2)  (0-1 percent of length, 1- number of bp) 
#2: minimum number of supporting caller, here we ask SURVIVOR only to report calls supported by 2 callers 
#1: take the type into account (1==yes, else no)
#1: take the strands of SVs into account (1==yes, else no)
#1: estimate distance based on the size of SV (1==yes, else no).
#50: minimum size of SVs to be taken into account
#sample_merged.vcf: output (vcf file) name

# 3. Filter CNV

SURVIVOR filter ${fam}_merged_CNV.vcf /home/users/s/sararols/scratch/DDID_data/Resources/hg38/hg38.p14.alternate_chr.bed 50 -1 0.05 20 ${fam}_merged_filtered_CNV.vcf


#SURVIVOR filter test_merged.vcf bad_regions.bed 50 -1 0.05 10 my_SV.filtered.vcf 

#bad_regions.bed: filter based on coordinates - filter out SVs overlapping with certain regions
#50: filter based on on min size (here min 50bp, max not defined -1)
#-1: filter based on on max size (max not defined -1)
#0.01: filter based on allele frequency (here 0.05 -> 5%AF) 
#10: filter based on read depth (here min 10 reads) 
#my_SV.filtered.vcf: output vcf file name


#-- Comparison/filtering
#	merge	Compare or merge VCF files to generate a consensus or multi sample vcf files.
#	filter	Filter a vcf file based on size and/or regions to ignore
#	stats	Report multipe stats over a VCF file
#	compMUMMer	Annotates a VCF file with the breakpoints found with MUMMer (Show-diff).

# Identify regions of potential falsely called SV.

# SURVIVOR reports overlapping tags with SUPP= and SUPP_VEC= which represents the number of samples carrying the SV and which sample (0/1 per sample in the same order as the genotype columns). NOTE ./. or 0/0 is not counted as supporting a variant.

# 1. Extract the low mapping quality reads from our e.g. sorted bam file:

#samtools view -H our.sort.bam > lowMQ.sam
#samtools view our.sort.bam | awk '$5<5 {print $0}' >>  lowMQ.sam
#samtools view -S -b -h lowMQ.sam > lowMQ.bam
# Now we should have a sorted bam file of all the reads that have an MQ <5 (second line awk statement).

# 2. Compute the bp coverage:
#samtools depth lowMQ.bam >  lowMQ.cov

# 3. Use SURVIVOR to cluster the coverage track into a bed file for filtering:
#SURVIVOR bincov lowMQ.cov 10 2 > lowMQ.bed

#Here we allow to cluster regions together that are maximal 10bp away and only consider regions if their coverage is larger then 2. 
# Filter SV using the generated Bed file using bedtools.
