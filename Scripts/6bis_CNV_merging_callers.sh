#!/bin/bash
#SBATCH --job-name Merge_callers
#SBATCH --error Merge_callers-error.e%j
#SBATCH --output Merge_callers-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 11:00:00
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault
#SBATCH --mail-user=pauline.sararols@unige.ch

module load Anaconda3/5.3.0
. /opt/ebsofts/Core/Anaconda3/5.3.0/etc/profile.d/conda.sh
conda activate Survivor

sample="WGS-XX-X" 

### Merge different variant from different callers with Survivor ###

survivor_path="/home/users/s/sararols/scratch/DDID_data/Survivor/samples/${sample}"

cd $survivor_path

# 1. Create a txt file with vcf files to merge

# 2. Run Survivor 

SURVIVOR merge ${sample}_vcf_files.txt 1000 2 1 1 1 50 ${sample}_merged_CNV.vcf

#SURVIVOR merge sample_files.vcf 1000 2 1 1 (0) 30 sample_merged.vcf
#1000: maximum allowed distance of 1kb, as measured pairwise between breakpoints (begin1 vs begin2, end1 vs end2)  (0-1 percent of length, 1- number of bp) 
#2: minimum number of supporting caller, here we ask SURVIVOR only to report calls supported by 2 callers 
#1: take the type into account (1==yes, else no)
#1: take the strands of SVs into account (1==yes, else no)
#1: estimate distance based on the size of SV (1==yes, else no).
#50: minimum size of SVs to be taken into account
#sample_merged.vcf: output (vcf file) name

# 3. Filter CNV

SURVIVOR filter ${sample}_merged_CNV.vcf /home/users/s/sararols/scratch/DDID_data/Resources/hg38/hg38.p14.alternate_chr.bed 50 -1 0.05 20 ${sample}_merged_filtered_CNV.vcf

# Keep only FORMAT info from Delly and CNVnator
#cut ${sample}_merged_filtered_CNV.vcf -f 1,2,3,4,5,6,7,8,9,11,12 > ${sample}_CNV_final.vcf

