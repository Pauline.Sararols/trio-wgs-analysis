#!/bin/bash
#SBATCH --job-name ERDS
#SBATCH --error ERDS-error.e%j
#SBATCH --output ERDS-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task=1        # CPU cores/threads
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

module load Anaconda3/5.3.0
. /opt/ebsofts/Core/Anaconda3/5.3.0/etc/profile.d/conda.sh
conda activate ERDS

sample="WGS-XX-X"

# Help
# perl $HOME/.conda/envs/ERDS/share/erds-1.1-1/erds_pipeline.pl

#You must define the following parameters.
#-o: followed by the path of output directory.(-o output_dir)
#	-b: followed by specified bam_file. (-b bam_file)
#	-v: followed by specified variant_file file called by SNV calling tools. (-v variant_file)
#	-r: followed by specified reference file. (-r reference_fasta_file)

ERDS_dir="$HOME/.conda/envs/ERDS/share/erds-1.1-1"
Bam_dir="/home/users/s/sararols/scratch/DDID_data/Bam_T2T/${sample}"
SNV_dir="/home/users/s/sararols/scratch/DDID_data/DeepVariant/${sample}"
Output_dir="/home/users/s/sararols/scratch/DDID_data/ERDS/${sample}"

perl ${ERDS_dir}/erds_pipeline.pl -b ${Bam_dir}/${sample}.final.bam -v ${SNV_dir}/${sample}.variants.decomposed.vcf -o ${Output_dir} -r /home/users/s/sararols/scratch/DDID_data/Resources/T2T/chm13v2.0_maskedY.fa
