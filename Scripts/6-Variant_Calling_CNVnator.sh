#!/bin/bash
#SBATCH --job-name CNVnator
#SBATCH --error CNVnator-error.e%j
#SBATCH --output CNVnator-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 6:00:00
#SBATCH --cpus-per-task=16       # CPU cores/threads
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault

module load Anaconda3/5.3.0
. /opt/ebsofts/Core/Anaconda3/5.3.0/etc/profile.d/conda.sh
conda activate CNVnator

samle="WGS-XX-X"

BAM_DIR="/home/users/s/sararols/scratch/DDID_data/Bam/${sample}"
SplitGenomeDir="/home/users/s/sararols/scratch/DDID_data/Resources/hg38/by_chromosome_fa"

cd /home/users/s/sararols/scratch/DDID_data/CNVnator/${sample}

### 1. Get read-depth data ###
# EXTRACTING READ MAPPING FROM BAM/SAM FILES #
# For hg38 assembly
cnvnator -root ${sample}.root -tree ${BAM_DIR}/${sample}.final.bam -chrom chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 chr22 chrX chrY \

### 2. Predicting CNV regions ###
# Calculate RD histograms
#cnvnator -root ${sample}.root -his 100 1000 10000 -fasta /home/users/s/sararols/scratch/DDID_data/Resources/hg38/Homo_sapiens_assembly38.fasta 
#cnvnator -root ${sample}.root -his 1000 -d ${SplitGenomeDir}
cnvnator -root ${sample}.root -his 100 -d ${SplitGenomeDir}

# bin 1000, 10000 and 100000) 

# 3. Calculate statistics
#cnvnator -root ${sample}.root -stat 1000
cnvnator -root ${sample}.root -stat 100

# 4. Partition (RD SIGNAL PARTITIONING - most time consuming step)
#cnvnator -root ${sample}.root -partition 1000
cnvnator -root ${sample}.root -partition 100

# 5. Call CNVs
cnvnator -root ${sample}.root -call 100 > ${sample}_CNVnator_100.txt
#cnvnator -root ${sample}.root -call 1000 > ${sample}_CNVnator_1000.txt
#cnvnator -root ${sample}.root -call 10000 > ${sample}_CNVnator_10000.vcf


# Convert results into vcf file
perl /home/users/s/sararols/scratch/DDID_data/Scripts/CNVnator2vcf.pl -prefix ${sample}.100 -reference hg38 -id 1 ${sample}_CNVnator_100.txt ${SplitGenomeDir} > ${sample}_CNVnator.vcf


# Merging root files
#cnvnator -root out.root -merge file1.root file2.root

### Import mask data
#To mark variants based on genome accessibility using mask file from the 1000 Genomes Project:

#cnvnator -root file.root -mask mask.fa.gz
#cnvnator -root file.root [-chrom name1 ...] [-rmchr | -addchr] -mask maskfile.fa.gz
