#!/bin/bash
#SBATCH --job-name Mapping
#SBATCH --error Mapping-error.e%j
#SBATCH --output Mapping-out.o%j
#SBATCH --partition public-cpu
#SBATCH --time 2-00:00:00
#SBATCH --cpus-per-task 16
#SBATCH --mem=60000              # 60Go

module purge
module load GCC/8.3.0 BWA/0.7.17
module load GCC/8.3.0 SAMtools/1.10

sample="WGS-XX-X"

#resources="/home/users/s/sararols/scratch/DDID_data/Resources/T2T/"
resources="/home/users/s/sararols/scratch/DDID_data/Resources/hg38/"

cd /home/users/s/sararols/scratch/DDID_data/Bam/${sample}

bwa mem -t 16 ${resources}/Homo_sapiens_assembly38.fasta /home/users/s/sararols/scratch/DDID_data/Fastq/${sample}/${sample}_R1.fastq.gz /home/users/s/sararols/scratch/DDID_data/Fastq/${sample}/${sample}_R2.fastq.gz | samtools view -bS -@16 - > ${sample}.unsorted.bam
samtools sort ${sample}.unsorted.bam -o ${sample}.bam
samtools index ${sample}.bam
samtools stats ${sample}.bam > ${sample}.CovStats