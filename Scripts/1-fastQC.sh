#!/bin/bash
#SBATCH --job-name FastQC
#SBATCH --error FastQC-error.e%j
#SBATCH --output FastQC-out.o%j
#SBATCH --partition shared-cpu
#SBATCH --time 12:00:00
#SBATCH --cpus-per-task 8
#SBATCH --mem=60000              # 60Go
#SBATCH --mail-type=all          # send email on job start, end and fault
#SBATCH --mail-user=pauline.sararols@unige.ch

sample="WGS-11-3"

module load FastQC/0.11.5-Java-1.8.0_74

cd /home/users/s/sararols/scratch/DDID_data/Fastq/${sample}

fastqc --noextract -f fastq ${sample}_R1.fastq.gz
fastqc --noextract -f fastq ${sample}_R2.fastq.gz